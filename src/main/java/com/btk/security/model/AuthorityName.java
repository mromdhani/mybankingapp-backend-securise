package com.btk.security.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}